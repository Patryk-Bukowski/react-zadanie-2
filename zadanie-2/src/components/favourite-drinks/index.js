import FavouriteDrinkItem from '../favourite-drink-item';

const FavouriteDrinks = ({list}) => {
    return (
        <ul>
            {list.map((item, index)=> <FavouriteDrinkItem name={item} key={index} />)}
        </ul>
    );
}
export default FavouriteDrinks;