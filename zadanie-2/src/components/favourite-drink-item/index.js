const FavouriteDrinkItem = ({name}) => {
    return (
        <li>
            {name}
        </li>
    );
}
export default FavouriteDrinkItem;