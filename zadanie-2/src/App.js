import './App.css';
import FavouriteDrinks from './components/favourite-drinks';

function App() {
  const list = ['cola','fanta','sprite','yerba mate', 'woda'];
  return (
    <div className="App">
      <header className="App-header">
        <FavouriteDrinks list={list} />
      </header>
    </div>
  );
}

export default App;
